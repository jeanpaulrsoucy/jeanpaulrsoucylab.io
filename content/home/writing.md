+++
widget = "blank"  # See https://sourcethemes.com/academic/docs/page-builder/
headless = true  # This file represents a page section.
active = true  # Activate this widget? true/false
weight = 30  # Order that this section will appear.

title = "Writing"

+++

- AI could revitalize local journalism, but only if we demand better digital municipal records ([The Globe and Mail](https://www.theglobeandmail.com/opinion/article-ai-could-revitalize-local-journalism-but-only-if-we-demand-better/), 2023)
- Canada needs better data to confront our health-care crisis ([The Hub](https://thehub.ca/2022-08-10/jean-paul-soucy-canada-needs-better-data-to-confront-our-health-care-crisis/), 2022)
- Why we must not ‘follow the science’ ([The Hub](https://thehub.ca/2022-06-27/jean-paul-r-soucy-why-we-must-not-follow-the-science/), 2022)
