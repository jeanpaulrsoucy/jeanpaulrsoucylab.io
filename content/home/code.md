+++
widget = "blank"  # See https://sourcethemes.com/academic/docs/page-builder/
headless = true  # This file represents a page section.
active = true  # Activate this widget? true/false
weight = 85  # Order that this section will appear.

title = "Code"

+++

As a proponent of reproducible research, many of my research papers are accompanied by [data and code on GitHub](https://github.com/jeanpaulrsoucy/#reproducible-research).

**COVID-19 Canada Open Data Working Group**

I built a series of [R packages](https://github.com/ccodwg/Covid19CanadaBot) to automatically update the COIVD-19 Canada Open Data Working Group dataset ([new dataset](https://github.com/ccodwg/CovidTimelineCanada) / [old dataset](https://github.com/ccodwg/Covid19Canada)) using GitHub Actions.

I also built a [Python package](https://github.com/jeanpaulrsoucy/archivist) to automatically archive COVID-19 datasets, webpages, and documents, which is publicly available as the [Canadian COVID-19 Data Archive](https://github.com/ccodwg/Covid19CanadaArchive).

**Other R packages**

* [getDDD](https://github.com/jeanpaulrsoucy/getDDD): Get WHO Defined Daily Dose – Download defined daily dose (DDD) information for selected ATC codes from the website of the WHO Collaborating Centre for Drug Statistics Methodology ([www.whocc.no](https://www.whocc.no/)).
