---
title: "Just Some COVID Variant Data"
author: Jean-Paul R. Soucy
date: "2021-02-05"
slug: "covid-variant-data"
categories: ["blog"]
tags: ["covid-19", "open-data"]
subtitle: ""
summary: "A Canadian COVID variant tracker"
authors: []
lastmod: "2021-02-05"
featured: no
image:
  caption: ""
  focal_point: ""
  preview_only: yes
projects: ["covid-19"]
---

<script src="{{< blogdown/postref >}}index_files/header-attrs/header-attrs.js"></script>


<html>
<link rel="stylesheet" href="/css/post.css" />
</html>
<details>
<p><summary>Short on time? Click here for the TL;DR</summary></p>
<p>CTV News has released Canada’s <a href="https://www.ctvnews.ca/health/coronavirus/tracking-variants-of-the-novel-coronavirus-in-canada-1.5296141" target="_blank">first COVID variant tracker</a>. Thanks to <a href="https://twitter.com/jessetahirali" target="_blank">Jesse Tahirali
</a> and <a href="https://twitter.com/_stephanieliu" target="_blank">Stephanie Liu
</a> for curating this invaluable resource.</p>
<p>I scraped the underlying time series, which I plotted to show the progression of COVID variant detection in Canada.</p>
<p>Detection of COVID variants will increase over the coming weeks as surveillance ramps up and as B.1.1.7 gains market share. B.1.1.7 is still growing exponentially, even as overall cases shrink.</p>
<p>The recent experience of the UK shows that variants can still be controlled using non-pharmaceutical measures, but this must begin with a recognition of the importance of airborne transmission.</p>
</details>
<div id="ctv-releases-canadas-first-covid-variant-tracker" class="section level2">
<h2>CTV releases Canada’s first COVID variant tracker</h2>
<p>Last night, CTV News launched a new <a href="https://www.ctvnews.ca/health/coronavirus/tracking-variants-of-the-novel-coronavirus-in-canada-1.5296141">COVID variant tracker</a>, curated by <a href="https://twitter.com/jessetahirali">Jesse Tahirali</a> and <a href="https://twitter.com/_stephanieliu">Stephanie Liu</a>. The variants covered by this tracker comprise B.1.1.7 (“the UK variant”), B.1.351 (“the South African variant”) and P1 (“the Brazilian variant”). The common names for these variants derive from the first nation where they were detected, not necessarily where these variants first arose. The UK, for example, has <a href="https://www.nature.com/articles/d41586-021-00065-4">excellent genomic surveillance</a>, making it particularly likely to discover new virus variants.</p>
</div>
<div id="scraping-the-underlying-data" class="section level2">
<h2>Scraping the underlying data</h2>
<p>For the moment, CTV’s tracker only displays the cumulative number of variants detected in each province. However, the underlying data are <a href="https://beta.ctvnews.ca/content/dam/common/exceltojson/COVID-Variants.txt">stored as a time series in JSON</a>. We can scrape it and turn it into a usable table using a little R code.</p>
<pre class="r"><code># load packages
library(jsonlite)
suppressPackageStartupMessages(library(dplyr))
library(tidyr)

# load and process data
variants &lt;- fromJSON(&quot;https://beta.ctvnews.ca/content/dam/common/exceltojson/COVID-Variants.txt&quot;, flatten = FALSE) %&gt;%
  ## remove blank data and summary data
  filter(!Date %in% c(&quot;&quot;, &quot;Updated&quot;, &quot;Total&quot;)) %&gt;%
  ## convert Excel dates
  mutate(date = as.Date(as.integer(Date), origin = &quot;1899-12-30&quot;))

# create usable table
variants &lt;- bind_cols(
  select(variants, date, contains(&quot;B117&quot;)) %&gt;%
    pivot_longer(
      cols = ends_with(&quot;B117&quot;),
      names_to = c(&quot;province&quot;, &quot;.value&quot;),
      names_sep = &quot;_&quot;,
      values_to = &quot;B117&quot;,
      values_transform = list(B117 = as.integer)
    ) %&gt;%
    arrange(date, province) %&gt;%
    group_by(province) %&gt;%
    fill(3, .direction = &quot;down&quot;) %&gt;%
    ungroup,
  select(variants, date, contains(&quot;B1351&quot;)) %&gt;%
    pivot_longer(
      cols = ends_with(&quot;B1351&quot;),
      names_to = c(&quot;province&quot;, &quot;.value&quot;),
      names_sep = &quot;_&quot;,
      values_to = &quot;B1351&quot;,
      values_transform = list(B1351 = as.integer)
    ) %&gt;%
    arrange(date, province) %&gt;%
    group_by(province) %&gt;%
    fill(3, .direction = &quot;down&quot;) %&gt;%
    ungroup %&gt;%
    select(3),
  select(variants, date, contains(&quot;P1&quot;)) %&gt;%
    pivot_longer(
      cols = ends_with(&quot;P1&quot;),
      names_to = c(&quot;province&quot;, &quot;.value&quot;),
      names_sep = &quot;_&quot;,
      values_to = &quot;P1&quot;,
      values_transform = list(P1 = as.integer)
    ) %&gt;%
    arrange(date, province) %&gt;%
    group_by(province) %&gt;%
    fill(3, .direction = &quot;down&quot;) %&gt;%
    ungroup %&gt;%
    select(3)
) %&gt;%
  replace_na(list(B117 = 0, B1351 = 0, P1 = 0))

# preview table
tail(variants)</code></pre>
<pre><code>## # A tibble: 6 x 5
##   date       province  B117 B1351    P1
##   &lt;date&gt;     &lt;chr&gt;    &lt;dbl&gt; &lt;dbl&gt; &lt;dbl&gt;
## 1 2021-02-04 NU           0     0     0
## 2 2021-02-04 ON         152     1     0
## 3 2021-02-04 PE           0     0     0
## 4 2021-02-04 QC           8     0     0
## 5 2021-02-04 SK           3     0     0
## 6 2021-02-04 YT           0     0     0</code></pre>
<p>This provides us with a cumulative time series of detected variants by province and date.</p>
<p><img src="{{< blogdown/postref >}}index_files/figure-html/unnamed-chunk-2-1.png" width="672" /></p>
</div>
<div id="data-caveats" class="section level2">
<h2>Data caveats</h2>
<p>There are a few oddities with the data. For example, cumulative B.1.1.7 detections seem to fall from February 2 to February 3. However, this is entirely consistent with the Public Health Ontario daily reports from these days: on <a href="https://files.ontario.ca/moh-covid-19-report-en-2021-02-02.pdf">February 2</a>, the Simcoe Muskoka District Health Unit reported 51 cumulative detections; on <a href="https://files.ontario.ca/moh-covid-19-report-en-2021-02-03.pdf">February 3</a>, they reported 44 cumulative detections. This indicates either a data error or data correction occurred.</p>
<p><img src="{{< blogdown/postref >}}index_files/figure-html/unnamed-chunk-3-1.png" width="672" /></p>
<p>The first cases of the B.1.1.7 variant in Ontario were detected in <a href="https://www.cbc.ca/news/canada/toronto/ontario-confirms-first-cases-covid19-uk-variant-1.5855361">late December</a>. However, this database appears to pull primarily from the Public Health Ontario surveillance reports, which didn’t begin reporting variant detections until late January, which explains why these earlier numbers are missing.</p>
</div>
<div id="detection-of-covid-variants-will-increase" class="section level2">
<h2>Detection of COVID variants will increase</h2>
<p>Surveillance for COVID variants is increasing, so we should naturally expect detections to increase rapidly in the weeks. For example, <a href="https://news.ontario.ca/en/backgrounder/60172/ontario-takes-immediate-action-to-address-covid-19-variants">Public Health Ontario laboratories</a> will be screening all positive tests for variants as well as ramping genomic testing up to a minimum of 10% of all positive samples.</p>
<p>At the same time, the variant itself is gaining market share and will be responsible for an increasing fraction of infections.</p>
<p>{{% tweet "1356428705262931968" %}}</p>
</div>
<div id="conclusion" class="section level2">
<h2>Conclusion</h2>
<p>More transmissable variants are here. The recent experience of the UK shows that they can be controlled using the same non-pharmaceutical measures as regular COVID, only it is more difficult. <a href="https://www.cbc.ca/news/canada/montreal/experts-ask-provinces-update-health-guidelines-covid-aerosol-airborne-transmission-1.5860809">COVID is airborne</a>, and this recognition should be foundational to our strategy for combating COVID-19.</p>
<p>You can download the raw data from this post <a download='variants.csv' href=data:text/csv;base64,ImRhdGUiLCJwcm92aW5jZSIsIkIxMTciLCJCMTM1MSIsIlAxIgoyMDIxLTAxLTAxLCJBQiIsMCwwLDAKMjAyMS0wMS0wMSwiQkMiLDAsMCwwCjIwMjEtMDEtMDEsIkNhbmFkYSIsMCwwLDAKMjAyMS0wMS0wMSwiTUIiLDAsMCwwCjIwMjEtMDEtMDEsIk5CIiwwLDAsMAoyMDIxLTAxLTAxLCJOTCIsMCwwLDAKMjAyMS0wMS0wMSwiTlMiLDAsMCwwCjIwMjEtMDEtMDEsIk5UIiwwLDAsMAoyMDIxLTAxLTAxLCJOVSIsMCwwLDAKMjAyMS0wMS0wMSwiT04iLDAsMCwwCjIwMjEtMDEtMDEsIlBFIiwwLDAsMAoyMDIxLTAxLTAxLCJRQyIsMCwwLDAKMjAyMS0wMS0wMSwiU0siLDAsMCwwCjIwMjEtMDEtMDEsIllUIiwwLDAsMAoyMDIxLTAxLTAyLCJBQiIsMCwwLDAKMjAyMS0wMS0wMiwiQkMiLDAsMCwwCjIwMjEtMDEtMDIsIkNhbmFkYSIsMCwwLDAKMjAyMS0wMS0wMiwiTUIiLDAsMCwwCjIwMjEtMDEtMDIsIk5CIiwwLDAsMAoyMDIxLTAxLTAyLCJOTCIsMCwwLDAKMjAyMS0wMS0wMiwiTlMiLDAsMCwwCjIwMjEtMDEtMDIsIk5UIiwwLDAsMAoyMDIxLTAxLTAyLCJOVSIsMCwwLDAKMjAyMS0wMS0wMiwiT04iLDAsMCwwCjIwMjEtMDEtMDIsIlBFIiwwLDAsMAoyMDIxLTAxLTAyLCJRQyIsMCwwLDAKMjAyMS0wMS0wMiwiU0siLDAsMCwwCjIwMjEtMDEtMDIsIllUIiwwLDAsMAoyMDIxLTAxLTAzLCJBQiIsMCwwLDAKMjAyMS0wMS0wMywiQkMiLDAsMCwwCjIwMjEtMDEtMDMsIkNhbmFkYSIsMCwwLDAKMjAyMS0wMS0wMywiTUIiLDAsMCwwCjIwMjEtMDEtMDMsIk5CIiwwLDAsMAoyMDIxLTAxLTAzLCJOTCIsMCwwLDAKMjAyMS0wMS0wMywiTlMiLDAsMCwwCjIwMjEtMDEtMDMsIk5UIiwwLDAsMAoyMDIxLTAxLTAzLCJOVSIsMCwwLDAKMjAyMS0wMS0wMywiT04iLDAsMCwwCjIwMjEtMDEtMDMsIlBFIiwwLDAsMAoyMDIxLTAxLTAzLCJRQyIsMCwwLDAKMjAyMS0wMS0wMywiU0siLDAsMCwwCjIwMjEtMDEtMDMsIllUIiwwLDAsMAoyMDIxLTAxLTA0LCJBQiIsMCwwLDAKMjAyMS0wMS0wNCwiQkMiLDAsMCwwCjIwMjEtMDEtMDQsIkNhbmFkYSIsMCwwLDAKMjAyMS0wMS0wNCwiTUIiLDAsMCwwCjIwMjEtMDEtMDQsIk5CIiwwLDAsMAoyMDIxLTAxLTA0LCJOTCIsMCwwLDAKMjAyMS0wMS0wNCwiTlMiLDAsMCwwCjIwMjEtMDEtMDQsIk5UIiwwLDAsMAoyMDIxLTAxLTA0LCJOVSIsMCwwLDAKMjAyMS0wMS0wNCwiT04iLDAsMCwwCjIwMjEtMDEtMDQsIlBFIiwwLDAsMAoyMDIxLTAxLTA0LCJRQyIsMCwwLDAKMjAyMS0wMS0wNCwiU0siLDAsMCwwCjIwMjEtMDEtMDQsIllUIiwwLDAsMAoyMDIxLTAxLTA1LCJBQiIsMCwwLDAKMjAyMS0wMS0wNSwiQkMiLDAsMCwwCjIwMjEtMDEtMDUsIkNhbmFkYSIsMCwwLDAKMjAyMS0wMS0wNSwiTUIiLDAsMCwwCjIwMjEtMDEtMDUsIk5CIiwwLDAsMAoyMDIxLTAxLTA1LCJOTCIsMCwwLDAKMjAyMS0wMS0wNSwiTlMiLDAsMCwwCjIwMjEtMDEtMDUsIk5UIiwwLDAsMAoyMDIxLTAxLTA1LCJOVSIsMCwwLDAKMjAyMS0wMS0wNSwiT04iLDAsMCwwCjIwMjEtMDEtMDUsIlBFIiwwLDAsMAoyMDIxLTAxLTA1LCJRQyIsMCwwLDAKMjAyMS0wMS0wNSwiU0siLDAsMCwwCjIwMjEtMDEtMDUsIllUIiwwLDAsMAoyMDIxLTAxLTA2LCJBQiIsMCwwLDAKMjAyMS0wMS0wNiwiQkMiLDAsMCwwCjIwMjEtMDEtMDYsIkNhbmFkYSIsMCwwLDAKMjAyMS0wMS0wNiwiTUIiLDAsMCwwCjIwMjEtMDEtMDYsIk5CIiwwLDAsMAoyMDIxLTAxLTA2LCJOTCIsMCwwLDAKMjAyMS0wMS0wNiwiTlMiLDAsMCwwCjIwMjEtMDEtMDYsIk5UIiwwLDAsMAoyMDIxLTAxLTA2LCJOVSIsMCwwLDAKMjAyMS0wMS0wNiwiT04iLDAsMCwwCjIwMjEtMDEtMDYsIlBFIiwwLDAsMAoyMDIxLTAxLTA2LCJRQyIsMCwwLDAKMjAyMS0wMS0wNiwiU0siLDAsMCwwCjIwMjEtMDEtMDYsIllUIiwwLDAsMAoyMDIxLTAxLTA3LCJBQiIsMCwwLDAKMjAyMS0wMS0wNywiQkMiLDAsMCwwCjIwMjEtMDEtMDcsIkNhbmFkYSIsMCwwLDAKMjAyMS0wMS0wNywiTUIiLDAsMCwwCjIwMjEtMDEtMDcsIk5CIiwwLDAsMAoyMDIxLTAxLTA3LCJOTCIsMCwwLDAKMjAyMS0wMS0wNywiTlMiLDAsMCwwCjIwMjEtMDEtMDcsIk5UIiwwLDAsMAoyMDIxLTAxLTA3LCJOVSIsMCwwLDAKMjAyMS0wMS0wNywiT04iLDAsMCwwCjIwMjEtMDEtMDcsIlBFIiwwLDAsMAoyMDIxLTAxLTA3LCJRQyIsMCwwLDAKMjAyMS0wMS0wNywiU0siLDAsMCwwCjIwMjEtMDEtMDcsIllUIiwwLDAsMAoyMDIxLTAxLTA4LCJBQiIsMCwwLDAKMjAyMS0wMS0wOCwiQkMiLDAsMCwwCjIwMjEtMDEtMDgsIkNhbmFkYSIsMCwwLDAKMjAyMS0wMS0wOCwiTUIiLDAsMCwwCjIwMjEtMDEtMDgsIk5CIiwwLDAsMAoyMDIxLTAxLTA4LCJOTCIsMCwwLDAKMjAyMS0wMS0wOCwiTlMiLDAsMCwwCjIwMjEtMDEtMDgsIk5UIiwwLDAsMAoyMDIxLTAxLTA4LCJOVSIsMCwwLDAKMjAyMS0wMS0wOCwiT04iLDAsMCwwCjIwMjEtMDEtMDgsIlBFIiwwLDAsMAoyMDIxLTAxLTA4LCJRQyIsMCwwLDAKMjAyMS0wMS0wOCwiU0siLDAsMCwwCjIwMjEtMDEtMDgsIllUIiwwLDAsMAoyMDIxLTAxLTA5LCJBQiIsMCwwLDAKMjAyMS0wMS0wOSwiQkMiLDAsMCwwCjIwMjEtMDEtMDksIkNhbmFkYSIsMCwwLDAKMjAyMS0wMS0wOSwiTUIiLDAsMCwwCjIwMjEtMDEtMDksIk5CIiwwLDAsMAoyMDIxLTAxLTA5LCJOTCIsMCwwLDAKMjAyMS0wMS0wOSwiTlMiLDAsMCwwCjIwMjEtMDEtMDksIk5UIiwwLDAsMAoyMDIxLTAxLTA5LCJOVSIsMCwwLDAKMjAyMS0wMS0wOSwiT04iLDAsMCwwCjIwMjEtMDEtMDksIlBFIiwwLDAsMAoyMDIxLTAxLTA5LCJRQyIsMCwwLDAKMjAyMS0wMS0wOSwiU0siLDAsMCwwCjIwMjEtMDEtMDksIllUIiwwLDAsMAoyMDIxLTAxLTEwLCJBQiIsMCwwLDAKMjAyMS0wMS0xMCwiQkMiLDAsMCwwCjIwMjEtMDEtMTAsIkNhbmFkYSIsMCwwLDAKMjAyMS0wMS0xMCwiTUIiLDAsMCwwCjIwMjEtMDEtMTAsIk5CIiwwLDAsMAoyMDIxLTAxLTEwLCJOTCIsMCwwLDAKMjAyMS0wMS0xMCwiTlMiLDAsMCwwCjIwMjEtMDEtMTAsIk5UIiwwLDAsMAoyMDIxLTAxLTEwLCJOVSIsMCwwLDAKMjAyMS0wMS0xMCwiT04iLDAsMCwwCjIwMjEtMDEtMTAsIlBFIiwwLDAsMAoyMDIxLTAxLTEwLCJRQyIsMCwwLDAKMjAyMS0wMS0xMCwiU0siLDAsMCwwCjIwMjEtMDEtMTAsIllUIiwwLDAsMAoyMDIxLTAxLTExLCJBQiIsMCwwLDAKMjAyMS0wMS0xMSwiQkMiLDAsMCwwCjIwMjEtMDEtMTEsIkNhbmFkYSIsMCwwLDAKMjAyMS0wMS0xMSwiTUIiLDAsMCwwCjIwMjEtMDEtMTEsIk5CIiwwLDAsMAoyMDIxLTAxLTExLCJOTCIsMCwwLDAKMjAyMS0wMS0xMSwiTlMiLDAsMCwwCjIwMjEtMDEtMTEsIk5UIiwwLDAsMAoyMDIxLTAxLTExLCJOVSIsMCwwLDAKMjAyMS0wMS0xMSwiT04iLDAsMCwwCjIwMjEtMDEtMTEsIlBFIiwwLDAsMAoyMDIxLTAxLTExLCJRQyIsMCwwLDAKMjAyMS0wMS0xMSwiU0siLDAsMCwwCjIwMjEtMDEtMTEsIllUIiwwLDAsMAoyMDIxLTAxLTEyLCJBQiIsMCwwLDAKMjAyMS0wMS0xMiwiQkMiLDAsMCwwCjIwMjEtMDEtMTIsIkNhbmFkYSIsMCwwLDAKMjAyMS0wMS0xMiwiTUIiLDAsMCwwCjIwMjEtMDEtMTIsIk5CIiwwLDAsMAoyMDIxLTAxLTEyLCJOTCIsMCwwLDAKMjAyMS0wMS0xMiwiTlMiLDAsMCwwCjIwMjEtMDEtMTIsIk5UIiwwLDAsMAoyMDIxLTAxLTEyLCJOVSIsMCwwLDAKMjAyMS0wMS0xMiwiT04iLDAsMCwwCjIwMjEtMDEtMTIsIlBFIiwwLDAsMAoyMDIxLTAxLTEyLCJRQyIsMCwwLDAKMjAyMS0wMS0xMiwiU0siLDAsMCwwCjIwMjEtMDEtMTIsIllUIiwwLDAsMAoyMDIxLTAxLTEzLCJBQiIsMCwwLDAKMjAyMS0wMS0xMywiQkMiLDAsMCwwCjIwMjEtMDEtMTMsIkNhbmFkYSIsMCwwLDAKMjAyMS0wMS0xMywiTUIiLDAsMCwwCjIwMjEtMDEtMTMsIk5CIiwwLDAsMAoyMDIxLTAxLTEzLCJOTCIsMCwwLDAKMjAyMS0wMS0xMywiTlMiLDAsMCwwCjIwMjEtMDEtMTMsIk5UIiwwLDAsMAoyMDIxLTAxLTEzLCJOVSIsMCwwLDAKMjAyMS0wMS0xMywiT04iLDAsMCwwCjIwMjEtMDEtMTMsIlBFIiwwLDAsMAoyMDIxLTAxLTEzLCJRQyIsMCwwLDAKMjAyMS0wMS0xMywiU0siLDAsMCwwCjIwMjEtMDEtMTMsIllUIiwwLDAsMAoyMDIxLTAxLTE0LCJBQiIsMCwwLDAKMjAyMS0wMS0xNCwiQkMiLDAsMCwwCjIwMjEtMDEtMTQsIkNhbmFkYSIsMCwwLDAKMjAyMS0wMS0xNCwiTUIiLDAsMCwwCjIwMjEtMDEtMTQsIk5CIiwwLDAsMAoyMDIxLTAxLTE0LCJOTCIsMCwwLDAKMjAyMS0wMS0xNCwiTlMiLDAsMCwwCjIwMjEtMDEtMTQsIk5UIiwwLDAsMAoyMDIxLTAxLTE0LCJOVSIsMCwwLDAKMjAyMS0wMS0xNCwiT04iLDAsMCwwCjIwMjEtMDEtMTQsIlBFIiwwLDAsMAoyMDIxLTAxLTE0LCJRQyIsMCwwLDAKMjAyMS0wMS0xNCwiU0siLDAsMCwwCjIwMjEtMDEtMTQsIllUIiwwLDAsMAoyMDIxLTAxLTE1LCJBQiIsMCwwLDAKMjAyMS0wMS0xNSwiQkMiLDQsMSwwCjIwMjEtMDEtMTUsIkNhbmFkYSIsNCwxLDAKMjAyMS0wMS0xNSwiTUIiLDAsMCwwCjIwMjEtMDEtMTUsIk5CIiwwLDAsMAoyMDIxLTAxLTE1LCJOTCIsMCwwLDAKMjAyMS0wMS0xNSwiTlMiLDAsMCwwCjIwMjEtMDEtMTUsIk5UIiwwLDAsMAoyMDIxLTAxLTE1LCJOVSIsMCwwLDAKMjAyMS0wMS0xNSwiT04iLDAsMCwwCjIwMjEtMDEtMTUsIlBFIiwwLDAsMAoyMDIxLTAxLTE1LCJRQyIsMCwwLDAKMjAyMS0wMS0xNSwiU0siLDAsMCwwCjIwMjEtMDEtMTUsIllUIiwwLDAsMAoyMDIxLTAxLTE2LCJBQiIsMCwwLDAKMjAyMS0wMS0xNiwiQkMiLDQsMSwwCjIwMjEtMDEtMTYsIkNhbmFkYSIsNCwxLDAKMjAyMS0wMS0xNiwiTUIiLDAsMCwwCjIwMjEtMDEtMTYsIk5CIiwwLDAsMAoyMDIxLTAxLTE2LCJOTCIsMCwwLDAKMjAyMS0wMS0xNiwiTlMiLDAsMCwwCjIwMjEtMDEtMTYsIk5UIiwwLDAsMAoyMDIxLTAxLTE2LCJOVSIsMCwwLDAKMjAyMS0wMS0xNiwiT04iLDAsMCwwCjIwMjEtMDEtMTYsIlBFIiwwLDAsMAoyMDIxLTAxLTE2LCJRQyIsMCwwLDAKMjAyMS0wMS0xNiwiU0siLDAsMCwwCjIwMjEtMDEtMTYsIllUIiwwLDAsMAoyMDIxLTAxLTE3LCJBQiIsMCwwLDAKMjAyMS0wMS0xNywiQkMiLDQsMSwwCjIwMjEtMDEtMTcsIkNhbmFkYSIsNCwxLDAKMjAyMS0wMS0xNywiTUIiLDAsMCwwCjIwMjEtMDEtMTcsIk5CIiwwLDAsMAoyMDIxLTAxLTE3LCJOTCIsMCwwLDAKMjAyMS0wMS0xNywiTlMiLDAsMCwwCjIwMjEtMDEtMTcsIk5UIiwwLDAsMAoyMDIxLTAxLTE3LCJOVSIsMCwwLDAKMjAyMS0wMS0xNywiT04iLDAsMCwwCjIwMjEtMDEtMTcsIlBFIiwwLDAsMAoyMDIxLTAxLTE3LCJRQyIsMCwwLDAKMjAyMS0wMS0xNywiU0siLDAsMCwwCjIwMjEtMDEtMTcsIllUIiwwLDAsMAoyMDIxLTAxLTE4LCJBQiIsMCwwLDAKMjAyMS0wMS0xOCwiQkMiLDQsMSwwCjIwMjEtMDEtMTgsIkNhbmFkYSIsNCwxLDAKMjAyMS0wMS0xOCwiTUIiLDAsMCwwCjIwMjEtMDEtMTgsIk5CIiwwLDAsMAoyMDIxLTAxLTE4LCJOTCIsMCwwLDAKMjAyMS0wMS0xOCwiTlMiLDAsMCwwCjIwMjEtMDEtMTgsIk5UIiwwLDAsMAoyMDIxLTAxLTE4LCJOVSIsMCwwLDAKMjAyMS0wMS0xOCwiT04iLDAsMCwwCjIwMjEtMDEtMTgsIlBFIiwwLDAsMAoyMDIxLTAxLTE4LCJRQyIsMCwwLDAKMjAyMS0wMS0xOCwiU0siLDAsMCwwCjIwMjEtMDEtMTgsIllUIiwwLDAsMAoyMDIxLTAxLTE5LCJBQiIsMCwwLDAKMjAyMS0wMS0xOSwiQkMiLDQsMSwwCjIwMjEtMDEtMTksIkNhbmFkYSIsNCwxLDAKMjAyMS0wMS0xOSwiTUIiLDAsMCwwCjIwMjEtMDEtMTksIk5CIiwwLDAsMAoyMDIxLTAxLTE5LCJOTCIsMCwwLDAKMjAyMS0wMS0xOSwiTlMiLDAsMCwwCjIwMjEtMDEtMTksIk5UIiwwLDAsMAoyMDIxLTAxLTE5LCJOVSIsMCwwLDAKMjAyMS0wMS0xOSwiT04iLDAsMCwwCjIwMjEtMDEtMTksIlBFIiwwLDAsMAoyMDIxLTAxLTE5LCJRQyIsMCwwLDAKMjAyMS0wMS0xOSwiU0siLDAsMCwwCjIwMjEtMDEtMTksIllUIiwwLDAsMAoyMDIxLTAxLTIwLCJBQiIsMCwwLDAKMjAyMS0wMS0yMCwiQkMiLDQsMSwwCjIwMjEtMDEtMjAsIkNhbmFkYSIsNCwxLDAKMjAyMS0wMS0yMCwiTUIiLDAsMCwwCjIwMjEtMDEtMjAsIk5CIiwwLDAsMAoyMDIxLTAxLTIwLCJOTCIsMCwwLDAKMjAyMS0wMS0yMCwiTlMiLDAsMCwwCjIwMjEtMDEtMjAsIk5UIiwwLDAsMAoyMDIxLTAxLTIwLCJOVSIsMCwwLDAKMjAyMS0wMS0yMCwiT04iLDAsMCwwCjIwMjEtMDEtMjAsIlBFIiwwLDAsMAoyMDIxLTAxLTIwLCJRQyIsMCwwLDAKMjAyMS0wMS0yMCwiU0siLDAsMCwwCjIwMjEtMDEtMjAsIllUIiwwLDAsMAoyMDIxLTAxLTIxLCJBQiIsMCwwLDAKMjAyMS0wMS0yMSwiQkMiLDQsMSwwCjIwMjEtMDEtMjEsIkNhbmFkYSIsNCwxLDAKMjAyMS0wMS0yMSwiTUIiLDAsMCwwCjIwMjEtMDEtMjEsIk5CIiwwLDAsMAoyMDIxLTAxLTIxLCJOTCIsMCwwLDAKMjAyMS0wMS0yMSwiTlMiLDAsMCwwCjIwMjEtMDEtMjEsIk5UIiwwLDAsMAoyMDIxLTAxLTIxLCJOVSIsMCwwLDAKMjAyMS0wMS0yMSwiT04iLDAsMCwwCjIwMjEtMDEtMjEsIlBFIiwwLDAsMAoyMDIxLTAxLTIxLCJRQyIsMCwwLDAKMjAyMS0wMS0yMSwiU0siLDAsMCwwCjIwMjEtMDEtMjEsIllUIiwwLDAsMAoyMDIxLTAxLTIyLCJBQiIsMCwwLDAKMjAyMS0wMS0yMiwiQkMiLDYsMywwCjIwMjEtMDEtMjIsIkNhbmFkYSIsNyw0LDAKMjAyMS0wMS0yMiwiTUIiLDAsMCwwCjIwMjEtMDEtMjIsIk5CIiwwLDAsMAoyMDIxLTAxLTIyLCJOTCIsMCwwLDAKMjAyMS0wMS0yMiwiTlMiLDEsMSwwCjIwMjEtMDEtMjIsIk5UIiwwLDAsMAoyMDIxLTAxLTIyLCJOVSIsMCwwLDAKMjAyMS0wMS0yMiwiT04iLDAsMCwwCjIwMjEtMDEtMjIsIlBFIiwwLDAsMAoyMDIxLTAxLTIyLCJRQyIsMCwwLDAKMjAyMS0wMS0yMiwiU0siLDAsMCwwCjIwMjEtMDEtMjIsIllUIiwwLDAsMAoyMDIxLTAxLTIzLCJBQiIsMCwwLDAKMjAyMS0wMS0yMywiQkMiLDYsMywwCjIwMjEtMDEtMjMsIkNhbmFkYSIsNyw0LDAKMjAyMS0wMS0yMywiTUIiLDAsMCwwCjIwMjEtMDEtMjMsIk5CIiwwLDAsMAoyMDIxLTAxLTIzLCJOTCIsMCwwLDAKMjAyMS0wMS0yMywiTlMiLDEsMSwwCjIwMjEtMDEtMjMsIk5UIiwwLDAsMAoyMDIxLTAxLTIzLCJOVSIsMCwwLDAKMjAyMS0wMS0yMywiT04iLDAsMCwwCjIwMjEtMDEtMjMsIlBFIiwwLDAsMAoyMDIxLTAxLTIzLCJRQyIsMCwwLDAKMjAyMS0wMS0yMywiU0siLDAsMCwwCjIwMjEtMDEtMjMsIllUIiwwLDAsMAoyMDIxLTAxLTI0LCJBQiIsMCwwLDAKMjAyMS0wMS0yNCwiQkMiLDYsMywwCjIwMjEtMDEtMjQsIkNhbmFkYSIsNyw0LDAKMjAyMS0wMS0yNCwiTUIiLDAsMCwwCjIwMjEtMDEtMjQsIk5CIiwwLDAsMAoyMDIxLTAxLTI0LCJOTCIsMCwwLDAKMjAyMS0wMS0yNCwiTlMiLDEsMSwwCjIwMjEtMDEtMjQsIk5UIiwwLDAsMAoyMDIxLTAxLTI0LCJOVSIsMCwwLDAKMjAyMS0wMS0yNCwiT04iLDAsMCwwCjIwMjEtMDEtMjQsIlBFIiwwLDAsMAoyMDIxLTAxLTI0LCJRQyIsMCwwLDAKMjAyMS0wMS0yNCwiU0siLDAsMCwwCjIwMjEtMDEtMjQsIllUIiwwLDAsMAoyMDIxLTAxLTI1LCJBQiIsMjAsNSwwCjIwMjEtMDEtMjUsIkJDIiw2LDMsMAoyMDIxLTAxLTI1LCJDYW5hZGEiLDI3LDksMAoyMDIxLTAxLTI1LCJNQiIsMCwwLDAKMjAyMS0wMS0yNSwiTkIiLDAsMCwwCjIwMjEtMDEtMjUsIk5MIiwwLDAsMAoyMDIxLTAxLTI1LCJOUyIsMSwxLDAKMjAyMS0wMS0yNSwiTlQiLDAsMCwwCjIwMjEtMDEtMjUsIk5VIiwwLDAsMAoyMDIxLTAxLTI1LCJPTiIsMCwwLDAKMjAyMS0wMS0yNSwiUEUiLDAsMCwwCjIwMjEtMDEtMjUsIlFDIiwwLDAsMAoyMDIxLTAxLTI1LCJTSyIsMCwwLDAKMjAyMS0wMS0yNSwiWVQiLDAsMCwwCjIwMjEtMDEtMjYsIkFCIiwyMCw1LDAKMjAyMS0wMS0yNiwiQkMiLDYsMywwCjIwMjEtMDEtMjYsIkNhbmFkYSIsMjcsOSwwCjIwMjEtMDEtMjYsIk1CIiwwLDAsMAoyMDIxLTAxLTI2LCJOQiIsMCwwLDAKMjAyMS0wMS0yNiwiTkwiLDAsMCwwCjIwMjEtMDEtMjYsIk5TIiwxLDEsMAoyMDIxLTAxLTI2LCJOVCIsMCwwLDAKMjAyMS0wMS0yNiwiTlUiLDAsMCwwCjIwMjEtMDEtMjYsIk9OIiwwLDAsMAoyMDIxLTAxLTI2LCJQRSIsMCwwLDAKMjAyMS0wMS0yNiwiUUMiLDAsMCwwCjIwMjEtMDEtMjYsIlNLIiwwLDAsMAoyMDIxLTAxLTI2LCJZVCIsMCwwLDAKMjAyMS0wMS0yNywiQUIiLDIwLDUsMAoyMDIxLTAxLTI3LCJCQyIsNiwzLDAKMjAyMS0wMS0yNywiQ2FuYWRhIiw3OCw5LDAKMjAyMS0wMS0yNywiTUIiLDAsMCwwCjIwMjEtMDEtMjcsIk5CIiwwLDAsMAoyMDIxLTAxLTI3LCJOTCIsMCwwLDAKMjAyMS0wMS0yNywiTlMiLDEsMSwwCjIwMjEtMDEtMjcsIk5UIiwwLDAsMAoyMDIxLTAxLTI3LCJOVSIsMCwwLDAKMjAyMS0wMS0yNywiT04iLDUxLDAsMAoyMDIxLTAxLTI3LCJQRSIsMCwwLDAKMjAyMS0wMS0yNywiUUMiLDAsMCwwCjIwMjEtMDEtMjcsIlNLIiwwLDAsMAoyMDIxLTAxLTI3LCJZVCIsMCwwLDAKMjAyMS0wMS0yOCwiQUIiLDIwLDUsMAoyMDIxLTAxLTI4LCJCQyIsNiwzLDAKMjAyMS0wMS0yOCwiQ2FuYWRhIiw3OCw5LDAKMjAyMS0wMS0yOCwiTUIiLDAsMCwwCjIwMjEtMDEtMjgsIk5CIiwwLDAsMAoyMDIxLTAxLTI4LCJOTCIsMCwwLDAKMjAyMS0wMS0yOCwiTlMiLDEsMSwwCjIwMjEtMDEtMjgsIk5UIiwwLDAsMAoyMDIxLTAxLTI4LCJOVSIsMCwwLDAKMjAyMS0wMS0yOCwiT04iLDUxLDAsMAoyMDIxLTAxLTI4LCJQRSIsMCwwLDAKMjAyMS0wMS0yOCwiUUMiLDAsMCwwCjIwMjEtMDEtMjgsIlNLIiwwLDAsMAoyMDIxLTAxLTI4LCJZVCIsMCwwLDAKMjAyMS0wMS0yOSwiQUIiLDMxLDYsMAoyMDIxLTAxLTI5LCJCQyIsNyw0LDAKMjAyMS0wMS0yOSwiQ2FuYWRhIiw5OCwxMSwwCjIwMjEtMDEtMjksIk1CIiwwLDAsMAoyMDIxLTAxLTI5LCJOQiIsMCwwLDAKMjAyMS0wMS0yOSwiTkwiLDAsMCwwCjIwMjEtMDEtMjksIk5TIiwxLDEsMAoyMDIxLTAxLTI5LCJOVCIsMCwwLDAKMjAyMS0wMS0yOSwiTlUiLDAsMCwwCjIwMjEtMDEtMjksIk9OIiw1MSwwLDAKMjAyMS0wMS0yOSwiUEUiLDAsMCwwCjIwMjEtMDEtMjksIlFDIiw4LDAsMAoyMDIxLTAxLTI5LCJTSyIsMCwwLDAKMjAyMS0wMS0yOSwiWVQiLDAsMCwwCjIwMjEtMDEtMzAsIkFCIiwzMSw2LDAKMjAyMS0wMS0zMCwiQkMiLDcsNCwwCjIwMjEtMDEtMzAsIkNhbmFkYSIsMTA0LDExLDAKMjAyMS0wMS0zMCwiTUIiLDAsMCwwCjIwMjEtMDEtMzAsIk5CIiwwLDAsMAoyMDIxLTAxLTMwLCJOTCIsMCwwLDAKMjAyMS0wMS0zMCwiTlMiLDEsMSwwCjIwMjEtMDEtMzAsIk5UIiwwLDAsMAoyMDIxLTAxLTMwLCJOVSIsMCwwLDAKMjAyMS0wMS0zMCwiT04iLDU3LDAsMAoyMDIxLTAxLTMwLCJQRSIsMCwwLDAKMjAyMS0wMS0zMCwiUUMiLDgsMCwwCjIwMjEtMDEtMzAsIlNLIiwwLDAsMAoyMDIxLTAxLTMwLCJZVCIsMCwwLDAKMjAyMS0wMS0zMSwiQUIiLDMxLDYsMAoyMDIxLTAxLTMxLCJCQyIsNyw0LDAKMjAyMS0wMS0zMSwiQ2FuYWRhIiwxMDUsMTEsMAoyMDIxLTAxLTMxLCJNQiIsMCwwLDAKMjAyMS0wMS0zMSwiTkIiLDAsMCwwCjIwMjEtMDEtMzEsIk5MIiwwLDAsMAoyMDIxLTAxLTMxLCJOUyIsMSwxLDAKMjAyMS0wMS0zMSwiTlQiLDAsMCwwCjIwMjEtMDEtMzEsIk5VIiwwLDAsMAoyMDIxLTAxLTMxLCJPTiIsNTgsMCwwCjIwMjEtMDEtMzEsIlBFIiwwLDAsMAoyMDIxLTAxLTMxLCJRQyIsOCwwLDAKMjAyMS0wMS0zMSwiU0siLDAsMCwwCjIwMjEtMDEtMzEsIllUIiwwLDAsMAoyMDIxLTAyLTAxLCJBQiIsNDQsNywwCjIwMjEtMDItMDEsIkJDIiwxNCw0LDAKMjAyMS0wMi0wMSwiQ2FuYWRhIiwxMzYsMTIsMAoyMDIxLTAyLTAxLCJNQiIsMCwwLDAKMjAyMS0wMi0wMSwiTkIiLDAsMCwwCjIwMjEtMDItMDEsIk5MIiwwLDAsMAoyMDIxLTAyLTAxLCJOUyIsMSwxLDAKMjAyMS0wMi0wMSwiTlQiLDAsMCwwCjIwMjEtMDItMDEsIk5VIiwwLDAsMAoyMDIxLTAyLTAxLCJPTiIsNjksMCwwCjIwMjEtMDItMDEsIlBFIiwwLDAsMAoyMDIxLTAyLTAxLCJRQyIsOCwwLDAKMjAyMS0wMi0wMSwiU0siLDAsMCwwCjIwMjEtMDItMDEsIllUIiwwLDAsMAoyMDIxLTAyLTAyLCJBQiIsNTAsNywwCjIwMjEtMDItMDIsIkJDIiwxNCw0LDAKMjAyMS0wMi0wMiwiQ2FuYWRhIiwxODcsMTMsMAoyMDIxLTAyLTAyLCJNQiIsMCwwLDAKMjAyMS0wMi0wMiwiTkIiLDMsMCwwCjIwMjEtMDItMDIsIk5MIiwwLDAsMAoyMDIxLTAyLTAyLCJOUyIsMSwxLDAKMjAyMS0wMi0wMiwiTlQiLDAsMCwwCjIwMjEtMDItMDIsIk5VIiwwLDAsMAoyMDIxLTAyLTAyLCJPTiIsMTA5LDEsMAoyMDIxLTAyLTAyLCJQRSIsMCwwLDAKMjAyMS0wMi0wMiwiUUMiLDgsMCwwCjIwMjEtMDItMDIsIlNLIiwyLDAsMAoyMDIxLTAyLTAyLCJZVCIsMCwwLDAKMjAyMS0wMi0wMywiQUIiLDUwLDcsMAoyMDIxLTAyLTAzLCJCQyIsMTQsNCwwCjIwMjEtMDItMDMsIkNhbmFkYSIsMTg0LDEzLDAKMjAyMS0wMi0wMywiTUIiLDAsMCwwCjIwMjEtMDItMDMsIk5CIiwzLDAsMAoyMDIxLTAyLTAzLCJOTCIsMCwwLDAKMjAyMS0wMi0wMywiTlMiLDEsMSwwCjIwMjEtMDItMDMsIk5UIiwwLDAsMAoyMDIxLTAyLTAzLCJOVSIsMCwwLDAKMjAyMS0wMi0wMywiT04iLDEwNiwxLDAKMjAyMS0wMi0wMywiUEUiLDAsMCwwCjIwMjEtMDItMDMsIlFDIiw4LDAsMAoyMDIxLTAyLTAzLCJTSyIsMiwwLDAKMjAyMS0wMi0wMywiWVQiLDAsMCwwCjIwMjEtMDItMDQsIkFCIiw2MSw3LDAKMjAyMS0wMi0wNCwiQkMiLDE0LDQsMAoyMDIxLTAyLTA0LCJDYW5hZGEiLDI0MiwxMywwCjIwMjEtMDItMDQsIk1CIiwwLDAsMAoyMDIxLTAyLTA0LCJOQiIsMywwLDAKMjAyMS0wMi0wNCwiTkwiLDAsMCwwCjIwMjEtMDItMDQsIk5TIiwxLDEsMAoyMDIxLTAyLTA0LCJOVCIsMCwwLDAKMjAyMS0wMi0wNCwiTlUiLDAsMCwwCjIwMjEtMDItMDQsIk9OIiwxNTIsMSwwCjIwMjEtMDItMDQsIlBFIiwwLDAsMAoyMDIxLTAyLTA0LCJRQyIsOCwwLDAKMjAyMS0wMi0wNCwiU0siLDMsMCwwCjIwMjEtMDItMDQsIllUIiwwLDAsMA==>here</a>.</p>
<p>You can discuss this post on <a href="https://twitter.com/JPSoucy/status/1357702329055776769">Twitter</a>.</p>
</div>
