---
title: Percent Positivity, Thresholds & Crystal Balls
author: Jean-Paul R. Soucy
date: "2020-11-17"
slug: "percent-positivity-thresholds-crystal-balls"
categories: ["blog"]
tags: ["covid-19"]
subtitle: ""
summary: "Why percent positivity is such a critical metric"
authors: []
lastmod: "2020-11-17"
featured: no
image:
  caption: ""
  focal_point: ""
  preview_only: yes
projects: ["covid-19"]
---

<script src="index_files/header-attrs/header-attrs.js"></script>


<html>
<link rel="stylesheet" href="/css/post.css" />
</html>
<details>
<p><summary>Short on time? Click here for the TL;DR</summary></p>
<p>Percent positivity (positive tests / total tests * 100%) is so important because it summarizes two signals in one number: outbreak size and the adequacy of testing. A growing outbreak or insufficient testing (or both) will cause percent positivity to rise.</p>
<p>Trying to compare percent positivity over time is more difficult when the underlying population being tested changes over time. Examples in Ontario, Canada include the switch to appointment-based testing, suspension of contact tracing and changing criteria for testing.</p>
<p>Ontario initially proposed 10% positivity as a threshold for category “red” restrictions. However, 10% positivity is indicative of uncontrolled, exponential growth, as demonstrated by the experience of Europe and the United States during the second wave of the pandemic.</p>
<p>We must not wait until 10% positivity to act. The rapid deterioration of the COVID-19 situation in Manitoba shows that every city and province in Canada is still vulnerable to the pandemic. We must respond proactively and decisively to avoid serious societal consequences.</p>
</details>
<div id="percent-positivity" class="section level2">
<h2>Percent positivity</h2>
<p>While it may be the daily case numbers that grab the headlines, it’s far from the most important metric to watch when determining the trajectory of COVID-19, as I’ve <a href="https://twitter.com/JPSoucy/status/1319047155915673602">written before</a>. Instead, we should be focusing on other metrics, like the 7-day rolling average, the number of outbreaks in long-term care facilities and percent positivity.</p>
<p>What makes percent positivity in particular such a useful metric is that it summarizes two distinct signals in a single number. The definition of percent positivity is simple: the number of positive tests (numerator) divided by the total number of test results reported (denominator), expressed as a percentage.</p>
</div>
<div id="what-affects-percent-positivity" class="section level2">
<h2>What affects percent positivity?</h2>
<p>Assuming the underlying population being tested remains constant, there are two ways for percent positivity to increase: if positive tests increase (numerator) or total tests decrease (denominator). Thus, a rising positivity rate signals either a growing outbreak or insufficient testing (or more likely, both). Since insufficient testing directly impacts our ability to isolate cases, identify contacts and break chains of transmission, both situations should raise alarm bells.</p>
<p>Put another way, we should expect percent positivity to remain constant over time if and only if:</p>
<ol style="list-style-type: decimal">
<li>The fraction of new cases detected and reported is constant.</li>
<li>The number of tests scales with the true number of new cases.</li>
</ol>
{{% figure src="pitzer.png" alt="Figure 1 from Pitzer et al. 2020" caption="When these two assumptions are true, percent positivity remains constant ([Pitzer et al. 2020](https://www.medrxiv.org/content/10.1101/2020.04.20.20073338v1))" %}}
<p>But what if our original assumption is wrong? What if the underlying population being tested does change over time? Let’s look at three recent examples of this from the Canadian province of Ontario.</p>
{{% figure src="kwan_percent_positivity_ontario.jpeg" alt="Rising percent positivity over time in Ontario by Dr. Jennifer Kwan (@jkwan_md)" caption="[Rising percent positivity over time in Ontario](https://twitter.com/jkwan_md/status/1328357728465670146) by Dr. Jennifer Kwan ([@jkwan_md](https://twitter.com/jkwan_md))" %}}
<p>The first is the <a href="https://globalnews.ca/news/7380500/ontario-appointment-covid19-testing/">switch from walk-in testing to appointment-based testing</a> at the beginning of October. The populations getting tested under these two regimes are almost certainly different. However, it is difficult to predict the effect on percent positivity, only that comparing before and after is somewhat apples to oranges.</p>
<p>The second is the <a href="https://toronto.ctvnews.ca/toronto-public-health-limits-contact-tracing-efforts-amid-soaring-covid-19-infections-1.5131741">suspension of contact tracing in Toronto</a>. This leads to a smaller fraction of cases being detected (particularly asymptomatic cases), but it also means fewer people without symptoms will be prompted to get tested. So again, it’s hard to predict the effect of this change on percent positivity.</p>
<p>The final change is the <a href="https://www.toronto.com/news-story/10208173-province-tightens-covid-19-testing-eligibility-criteria-expands-testing-capacity/">tightening of testing criteria</a> in response to a massive testing bottleneck to discourage low-risk asymptomatic individuals from seeking tests. This move likely raised percent positivity—which is actually a good thing. <strong>Given a fixed testing capacity</strong>, your testing strategy should aim to maximize percent positivity, because this means you are capturing the highest possible fraction of cases.</p>
<p>We can illustrate this principle with an extreme example. Suppose you have the capacity to run 100 tests per day. Normally, you would choose who to test based on symptoms and/or exposure history. If you instead tested people at random, you would end up with a lower percent positivity. A higher percent positivity means you are capturing a higher fraction of cases with your limited testing capacity. This is why the interpretation of changes in percent positivity over time is only straightforward if the underlying population being tested remains roughly the same.</p>
</div>
<div id="thresholds" class="section level2">
<h2>Thresholds</h2>
<p>Why is percent positivity front of mind for me right now? In early November, the Ontario government unveiled a <a href="https://www.ontario.ca/page/covid-19-response-framework-keeping-ontario-safe-and-open">five category, colour-coded COVID-19 response framework</a> based on a series of metrics including the daily case rate, health system capacity and contact tracing ability.</p>
{{% figure src="thresholds_original.jpeg" alt="The original thresholds from Ontario's COVID-19 response framework" caption="The original thresholds from Ontario's COVID-19 response framework" %}}
<p>While the idea of a transparent, predictable response framework based on sensible metrics is a good one, I along with many other experts <a href="https://www.thestar.com/politics/provincial/2020/11/03/ontario-unveils-new-covid-19-restriction-guidelines-as-cases-hit-a-record-1050.html">expressed surprise</a> at the thresholds used in the response framework, as well as the accompanying responses.</p>
<p>One of the most shocking elements was the 10% positivity threshold for the red “control” category. A 10% positivity rate indicates an out-of-control epidemic or woefully inadequate testing (or both), especially during cold/flu season when the demand for testing will increase due to respiratory symptoms from non-COVID respiratory tract infections.</p>
<p>When you reach 10% positivity, it’s far too late to act to prevent serious consequences.</p>
</div>
<div id="why-we-dont-have-to-wait" class="section level2">
<h2>Why we don’t have to wait</h2>
<p>There’s no reason to wait to see if this theory bears out in Ontario. Here in Canada, heading deeper into autumn and winter, we have a crystal ball to see the future if we don’t act quickly and decisively to keep positivity rates dramatically below 10%. That crystal ball is <a href="https://www.economist.com/briefing/2020/11/07/the-second-wave-of-covid-19-has-sent-much-of-europe-back-into-lockdown">Europe</a> (and many parts of the United States), which for this second wave of the pandemic have been several weeks ahead of us in terms of trajectory. The threat of total health system collapse has sent several countries into severe lockdowns, and restrictions are mounting across <a href="https://www.nytimes.com/live/2020/11/16/world/covid-19-coronavirus-updates">many regions of the United States</a>. <a href="https://twitter.com/ASPphysician">Dr. Andrew Morris</a> breaks down the cautionary tale of Belgium admirably in the below Twitter thread.</p>
{{% tweet "1324352711463088128" %}}
<p>There’s no reason to expect things work differently here. Every province and every city in Canada is still vulnerable to COVID-19. There is perhaps no better illustration of this than the province of Manitoba. Up until early October, the province was relatively lightly hit by the virus. After this point, cases started exploding in Winnipeg and across the province. As of mid-November, Manitoba has by far the highest rate of COVID-19 hospitalizations per capita in the country.</p>
<p>According to Doctors Manitoba, Winnipeg’s test positivity first hit 10% on <a href="https://web.archive.org/web/20201117160839/https://doctorsmanitoba.ca/2020/11/covid-19-update-November-10/">November 10</a>, although it had previously hit 9.7% as early as <a href="https://web.archive.org/web/20201101005602/https://doctorsmanitoba.ca/2020/10/covid-19-update-October-30/">October 30</a>. Let’s see where these dates fall on the case and hospitalization curves. Are these really the points where we should decide to take corrective action? (Note: Cases are for Winnipeg, hospitalizations are for the whole province because I couldn’t find data for just Winnipeg)</p>
<p><img src="index_files/figure-html/unnamed-chunk-5-1.png" width="672" /></p>
<p>You do not wait until 10% positivity to act—you are almost certainly very deep into uncontrolled, exponential growth at this point. For the record, Winnipeg moved into shutdown on <a href="https://www.cbc.ca/news/canada/manitoba/manitoba-covid-19-restrictions-1.5783472">November 2</a>, followed by the rest of the province on <a href="https://web.archive.org/web/20201110223044/https://news.gov.mb.ca/news/index.html?item=49737">November 12</a>, the <a href="https://web.archive.org/web/20201117174108/https://doctorsmanitoba.ca/2020/11/covid-19-update-November-12/">day province as a whole</a> hit an 11% positivity rate.</p>
</div>
<div id="whats-next" class="section level2">
<h2>What’s next?</h2>
<p>Thankfully for Ontario, it didn’t take long for the government to backtrack on the response framework and <a href="https://www.thestar.com/politics/provincial/2020/11/13/ontarios-covid-19-cases-dip-but-hospitalizations-are-rising.html">release revised thresholds</a>, with the red “control” threshold set at a <a href="https://www.thestar.com/news/gta/2020/11/11/ontario-rejected-its-own-public-health-agencys-advice-when-it-launched-its-colour-coded-plan-for-covid-19-restrictions.html">much more reasonable 2.5%</a>. It is, at the very least, a step in the right direction.</p>
{{% figure src="thresholds_revised.jpeg" alt="The revised thresholds from Ontario's COVID-19 response framework" caption="The revised thresholds from Ontario's COVID-19 response framework" %}}
<p>How about the rest of the thresholds in the revised framework? Well…that’s another question for another post.</p>
<p>You can discuss this post on <a href="https://twitter.com/JPSoucy/status/1328786443578331137">Twitter</a>.</p>
</div>
