---
# Display name
title: Jean-Paul R. Soucy

# Username (this should match the folder name)
authors:
- admin

# Is this the primary user of the site?
superuser: true

# Role/position
role: PhD candidate in Epidemiology at the University of Toronto

# Organizations/Affiliations
organizations:
- name: Dalla Lana School of Public Health, University of Toronto
  url: "https://www.dlsph.utoronto.ca/program/phd-epidemiology/"

# Short bio (displayed in user profile at end of posts)
bio: My research interests include infectious disease epidemiology, health policy, and open data.

interests:
- Infectious disease epidemiology
- COVID-19
- Antimicrobial resistance
- Public health surveillance
- Open data and transparency
- Health policy

education:
  courses:
  - course: PhD in Epidemiology
    institution: University of Toronto
    year: "2018–Present (expected defence date: January 2024)"
  - course: MSc in Epidemiology
    institution: McGill University
    year: 2016–2018
  - course: HBSc in Biology; Minor in Mathematics
    institution: University of Ottawa
    year: 2011–2016

# Social/Academic Networking
# For available icons, see: https://sourcethemes.com/academic/docs/page-builder/#icons
#   For an email link, use "fas" icon pack, "envelope" icon, and a link in the
#   form "mailto:your-email@example.com" or "#contact" for contact widget.
social:
- icon: envelope
  icon_pack: fas
  link: '#contact'  # For a direct email link, use "mailto:test@example.org".
- icon: twitter
  icon_pack: fab
  link: https://twitter.com/JPSoucy
- icon: youtube
  icon_pack: fab
  link: https://www.youtube.com/channel/UC82w-KnbA-WrzynroaDLDrg
- icon: linkedin
  icon_pack: fab
  link: https://www.linkedin.com/in/jean-paul-r-soucy/
- icon: google-scholar
  icon_pack: ai
  link: https://scholar.google.ca/citations?user=dfyXZ7MAAAAJ&hl
- icon: orcid
  icon_pack: ai
  link: https://orcid.org/0000-0002-8422-2326
- icon: github
  icon_pack: fab
  link: https://github.com/jeanpaulrsoucy
- icon: gitlab
  icon_pack: fab
  link: https://gitlab.com/jeanpaulrsoucy
- icon: cv
  icon_pack: ai
  link: files/cv_jprs.pdf

# Enter email to display Gravatar (if Gravatar enabled in Config)
email: ""

# Organizational groups that you belong to (for People widget)
#   Set this to `[]` or comment out if you are not using People widget.
# user_groups:
# - Researchers
# - Visitors
---

I am a [Vanier Scholar](http://www.vanier.gc.ca/en/scholar_search-chercheur_recherche_2019.html) pursuing a PhD in Epidemiology at the University of Toronto's [Dalla Lana School of Public Health](http://www.dlsph.utoronto.ca/) in Toronto, Canada. My research interests include infectious disease epidemiology, health policy, and open data, with a focus on COVID-19 and antimicrobial resistance. I am advised by [Dr. Kevin Brown](https://www.publichealthontario.ca/en/about/research/our-researchers/kevin-brown) at [Public Health Ontario](https://www.publichealthontario.ca/) and [Dr. David Fisman](http://www.dlsph.utoronto.ca/faculty-profile/fisman-david-n/) in the [Epidemiology Division](http://www.dlsph.utoronto.ca/division/epidemiology/).

During the COVID-19 pandemic, I was frequently interviewed as an expert for TV, radio, and print journalism. My [research](https://scholar.google.ca/citations?user=dfyXZ7MAAAAJ&hl) has been published in journals including *The Lancet Microbe*, the *Canadian Medical Association Journal*, the *American Journal of Epidemiology*, and *Clinical Infectious Diseases*. I have also [written](#writing) for the popular press on subjects ranging from public health policy, health data, and artificial intelligence. I also blog at [Data Gripes](https://data.gripe/).

I am a co-founder of the [COVID-19 Canada Open Data Working Group](https://opencovid.ca/) and a founding editor of the [University of Toronto Journal of Public Health](https://utjph.com/index.php/utjph).

My one page resume can be found [here](files/resume_jprs.pdf). A full list of my publications and talks can be found in my [CV](files/cv_jprs.pdf).
